const Scoreboard = require('../src/ScoreBoard.js')
const Team = require('../src/Team.js')

const assert = require('assert')

describe('Scoreboard', () => {

  let scoreboard, spain, switzerland 

  beforeEach( () => {
    spain = new Team("Spain")
    switzerland = new Team("Switzerland")
    scoreboard = new Scoreboard(switzerland, spain)
  })

  it('describes points as "Love", "Fifteen", "Thirty", and "Forty" respectively', () => {
    switzerland.points = 3
    spain.points = 1

    let result = scoreboard.show()

    assert.equal(result, 'Forty-Fifteen')
  })

  it('declares "Deuce" if a game is 4-4', () => {
    switzerland.points = 4
    spain.points = 4

    let result = scoreboard.show()

    assert.equal(result, 'Deuce')
  })

  it('declares "Advantage" if a game is 4-3', () => {
    switzerland.points = 3
    spain.points = 4

    let result = scoreboard.show()

    assert.equal(result, 'Advantage Spain')
  })

})