const assert = require('assert')
const Team = require('../src/Team.js')

  describe('Team', () => {
    it('allows teams to have one member',()=>{
        const team = new Team()
        const onePlayer = 1
  
        const players =  team.defineNumberOfPlayers(onePlayer)
  
        assert.equal(players, onePlayer)
  
      })

      it('allows teams to have two members',()=>{
        const team = new Team()
        const twoPlayers = 2

        const players = team.defineNumberOfPlayers(twoPlayers)
  
        assert.equal(players, twoPlayers)
      })

      it('initiates a game where both teams have a score of 0 points, games and sets',()=>{
        const team = new Team()
        
        assert.equal(team.obtainPoints(), 0 )
        assert.equal(team.obtainGames(), 0 )
        assert.equal(team.obtainSets(), 0 )
      })
    })
