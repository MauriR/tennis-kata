const Court = require('../src/Court.js')
const Team = require('../src/Team.js')

const assert = require('assert')

describe('Tennis Kata', () => {

  let court, spain, switzerland 

  beforeEach( () => {
    spain = new Team("Spain")
    switzerland = new Team("Switzerland")
    court = new Court(spain, switzerland)
  })

  describe('Court', () => {
    it('has two teams', () => {
      const numberOfTeams = court.obtainTeams()

      assert.equal(numberOfTeams, 2)
    })

    it('both teams have the same number of players', () => {
      const twoPlayers = 2

      const expectSamePlayers =court.applySamePlayersPerTeam(twoPlayers)

      assert.equal(expectSamePlayers, true)
    })
  })

  describe('Game', () => {

    it('A team must have won at least two points more than its rival', () => {
      const gameWon = 1

      court.score(switzerland)
      court.score(switzerland)
      court.score(spain)
      court.score(switzerland)
      court.score(switzerland)

      assert.equal(switzerland.obtainGames(), gameWon)
    })
  })

  describe('Set', () => {

    it('a team wins a set if has at least six games and two more than the rival', () => {
      switzerland.points = 1
      spain.points = 3
    
      switzerland.games = 4
      spain.games = 5

      court.score(spain)

      assert.equal(spain.obtainSets(), 1)

    })

    it('If a player has won 6 games and the opponent 5, another game is played', () => {
      switzerland.points = 1
      spain.points = 3

      switzerland.games = 5
      spain.games = 5

      court.score(spain)

      assert.equal(spain.obtainSets(), 0)
    })

    it('allows a player to win a set 7–5', () => {
      switzerland.points = 1
      spain.points = 3

      switzerland.games = 5
      spain.games = 6

      court.score(spain)

      assert.equal(spain.obtainSets(), 1)

    })

    it('declares tie-break on 6-6', () => {
      switzerland.points = 3
      spain.points = 1

      switzerland.games = 5
      spain.games = 6

      court.score(switzerland)

      result = court.isAtTieBreak

      assert.equal(result, true)

    })

    it("grants the Tie-break winning team  the final point and the set", () => {
      court.isAtTieBreak = true

      switzerland.sets = 0
      spain.sets = 0

      switzerland.games = 6
      spain.games = 6

      switzerland.points = 7
      spain.points = 6

      court.score(switzerland)

      assert.equal(switzerland.obtainSets(), 1)
    })
  })

  describe('Match', () => {

    it("declares as a winner the first team on winning two sets", () => {
      switzerland.sets = 0
      spain.sets = 1

      switzerland.games = 4
      spain.games = 5

      switzerland.points = 1
      spain.points = 3

      winner = court.score(spain)

      assert.equal(winner, "Spain Wins")
    })
    
  })

})

