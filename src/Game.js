const DIFFERENCE_TO_WIN = 2
const MINIMUM_POINTS = 4 

class Game {
  win(spainPoints, switzerlandPoints) {
    if (this._existsGameCondition(spainPoints, switzerlandPoints)) {
      return true
    }
  }

  _existsGameCondition(spainPoints, switzerlandPoints) {
    let result = false

    if ((switzerlandPoints >= spainPoints + DIFFERENCE_TO_WIN) && (switzerlandPoints >= MINIMUM_POINTS)) {
      result = true
    }
    if ((spainPoints >= switzerlandPoints + DIFFERENCE_TO_WIN) && (spainPoints >= MINIMUM_POINTS)) {
      result = true
    }
    return result

  }
}

module.exports = Game