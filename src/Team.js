const TWO_PLAYERS = 2
const ONE_PLAYER = 1
const INITIAL_SCORE = 0

class Team {

    constructor(name) {
        this.name = name
        this.numberOfPlayers = null
        this.points = INITIAL_SCORE
        this.games = INITIAL_SCORE
        this.sets = INITIAL_SCORE
    }

    defineNumberOfPlayers(numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers
        if (numberOfPlayers == TWO_PLAYERS) {
            return TWO_PLAYERS
        }
        return ONE_PLAYER
    }

    startGame() {
        this.games = 0
        this.points = 0
        this.sets = 0
    }

    obtainPoints() {
        return this.points
    }
    
    obtainGames() {
        return this.games
    }

    obtainSets() {
        return this.sets
    }

    scoreAPoint(currentPoints = this.points) {
        this.points = currentPoints + 1
        return this.points
    }

    addGame(){
        this.games++
    }

    addSet(){
        this.sets++
    }

    reset(){
        this.points = 0
        this.games = 0
    }

    winTheMatch(){
        return this.name
    }
}


module.exports = Team