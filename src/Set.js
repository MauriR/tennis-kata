const DIFFERENCE_TO_WIN = 2
const MINIMUM_POINTS = 4

class Set {
  win(spainGames, switzerlandGames) {
    
    if (this._existsSetCondition(spainGames, switzerlandGames)) {
      return true
    }
  }

  _existsSetCondition(spainGames, switzerlandGames) {
    let result = false

    if ((switzerlandGames >= (spainGames + DIFFERENCE_TO_WIN)) && (switzerlandGames >= MINIMUM_POINTS)) {
      result = true
    }
    if ((spainGames >= (switzerlandGames + DIFFERENCE_TO_WIN)) && (spainGames >= MINIMUM_POINTS)) {
      result = true
    }
    return result
  }
}

module.exports = Set