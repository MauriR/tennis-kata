const DIFFERENCE_TO_WIN = 2
const MINIMUM_POINTS = 6

class TieBreak {

  game(spainPoints, switzerlandPoints) {
    if ((switzerlandPoints >= spainPoints + DIFFERENCE_TO_WIN) && (switzerlandPoints >= MINIMUM_POINTS)) {
      return true
    }
    if ((spainPoints >= switzerlandPoints + DIFFERENCE_TO_WIN) && (spainPoints >= MINIMUM_POINTS)) {
      return true
    }
  }
}

module.exports = TieBreak