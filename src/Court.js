const Game = require('./Game.js')
const Set = require('./Set.js')
const TieBreak = require('./TieBreak.js')

const TIE_BREAK_GAME = 6

class Court {
	constructor(firstTeam, secondTeam) {

		this.spain = firstTeam
		this.switzerland = secondTeam

		this.game = new Game()
		this.set = new Set()
		this.tieBreak = new TieBreak()

		this.isAtTieBreak = null
	}

	obtainTeams() {
		let teams = []
		teams.push(this.spain, this.switzerland)
		
		return teams.length
	}

	applySamePlayersPerTeam(players) {
		const spainPlayers = this.spain.defineNumberOfPlayers(players)
		const switzerlandPlayers = this.switzerland.defineNumberOfPlayers(players)

		return (spainPlayers == switzerlandPlayers)
	}

	score(team){
		team.scoreAPoint()

		this._winGame(team)

		const winner = this._winSet(team)

		return winner
	}

	_winGame(team){
		if (this.isAtTieBreak) {	
			if (this.tieBreak.game(this.spain.obtainPoints(), this.switzerland.obtainPoints())) {
				team.addSet()
				this._resetSet()
			}
		}
		else if (this.game.win(this.spain.obtainPoints(), this.switzerland.obtainPoints())) {
			team.addGame()
		}
				
		this._playTieBreak()
	}

	_winSet(team){
		if (this.set.win(this.spain.obtainGames(), this.switzerland.obtainGames())) {
			team.addSet()
			if (team.obtainSets() === 2) {
				const name = team.winTheMatch()
				return name + " Wins"
			}
		}
	}
	
	_resetSet() {
		this.isAtTieBreak = false
		this.spain.reset()
		this.switzerland.reset()
	}

	_playTieBreak() {
		if ((this.spain.obtainGames() == TIE_BREAK_GAME) && (this.switzerland.obtainGames() == TIE_BREAK_GAME)) {
			this.isAtTieBreak = true
		}
	}
}

module.exports = Court