const FORTY = 3

class ScoreBoard {

  constructor(switzerland, spain){
    this.switzerland = switzerland
    this.spain = spain
  }

  show() {
		let pointsInWords = {
			0: 'Love',
			1: 'Fifteen',
			2: 'Thirty',
			3: 'Forty'
		}

		if (this._haveBothTeams40orMore()) {
			return this._deuceOrAdvantage()
		}

		return `${pointsInWords[this.switzerland.obtainPoints()]}-${pointsInWords[this.spain.obtainPoints()]}`
	}

	_haveBothTeams40orMore() {
		return (this.switzerland.obtainPoints() >= FORTY && this.spain.obtainPoints() >= FORTY)
	}

	_deuceOrAdvantage() {
		if (this.switzerland.obtainPoints() - 1 == this.spain.obtainPoints()) {
			return 'Advantage Switzerland'
		} else if (this.switzerland.obtainPoints() + 1 == this.spain.obtainPoints()) {
			return 'Advantage Spain'
		} else {
			return 'Deuce'
		}
	}
}

module.exports = ScoreBoard